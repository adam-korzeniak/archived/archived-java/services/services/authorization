package com.adamkorzeniak.authorization.infrastructure.persistence;

import com.adamkorzeniak.authorization.domain.model.User;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserEntityMapper {

    User mapToDomain(UserEntity userEntity);

    @Mapping(target = "id", ignore = true)
    UserEntity mapToEntity(User user);

}
