package com.adamkorzeniak.authorization.domain;

import com.adamkorzeniak.authorization.domain.exception.model.UserDuplicateException;
import com.adamkorzeniak.authorization.domain.model.User;
import com.adamkorzeniak.authorization.infrastructure.persistence.UserPersistenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserPersistenceService userPersistenceService;
    private final PasswordEncoder encoder;

    public Optional<User> getUser(Principal principal) {
        return userPersistenceService.getUser(principal.getName());
    }

    public User register(User user) {
        validateIfNewUser(user.getUsername());
        user.setPassword(encoder.encode(user.getPassword()));
        return userPersistenceService.saveUser(user);
    }

    private void validateIfNewUser(String username) {
        if (userPersistenceService.existsUser(username)) {
            throw new UserDuplicateException(username);
        }
    }
}
