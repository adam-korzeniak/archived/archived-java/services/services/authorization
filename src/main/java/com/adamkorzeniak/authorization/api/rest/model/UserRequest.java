package com.adamkorzeniak.authorization.api.rest.model;

import lombok.EqualsAndHashCode;
import lombok.Generated;
import lombok.ToString;
import lombok.Value;

import javax.validation.constraints.NotBlank;

@Value
@Generated
public class UserRequest {

    @NotBlank
    String username;

    @NotBlank
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    String password;

    String role;
}
