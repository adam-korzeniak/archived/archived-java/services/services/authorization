package com.adamkorzeniak.authorization.api.rest.mapper;

import com.adamkorzeniak.authorization.api.rest.model.UserRequest;
import com.adamkorzeniak.authorization.api.rest.model.UserResponse;
import com.adamkorzeniak.authorization.domain.model.User;
import com.adamkorzeniak.authorization.test.utils.TestData;
import com.adamkorzeniak.authorization.test.utils.builder.UserBuilder;
import com.adamkorzeniak.authorization.test.utils.builder.UserRequestBuilder;
import com.adamkorzeniak.utils.security.infrastructure.model.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
class UserApiMapperTest {

    @Autowired
    private UserApiMapper userApiMapper;

    @Test
    void MapRequestToDomain() {
        UserRequest request = UserRequestBuilder.sample().build();

        User user = userApiMapper.mapRequest(request);

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(TestData.ADMIN_USERNAME);
        assertThat(user.getPassword()).isEqualTo(TestData.PASSWORD);
        assertThat(user.getRole()).isEqualTo(Role.ADMIN);
        assertThat(user.isEnabled()).isTrue();
    }

    @Test
    void MapRequestToDomainForEmptyRole() {
        UserRequest request = UserRequestBuilder.sample().role(null).build();

        User user = userApiMapper.mapRequest(request);

        assertThat(user).isNotNull();
        assertThat(user.getUsername()).isEqualTo(TestData.ADMIN_USERNAME);
        assertThat(user.getPassword()).isEqualTo(TestData.PASSWORD);
        assertThat(user.getRole()).isEqualTo(Role.USER);
        assertThat(user.isEnabled()).isTrue();
    }

    @Test
    void MapDomainToResponse() {
        User user = UserBuilder.sample().build();

        UserResponse response = userApiMapper.mapResponse(user);

        assertThat(response).isNotNull();
        assertThat(response.getUsername()).isEqualTo(TestData.ADMIN_USERNAME);
        assertThat(response.getRole()).isEqualTo(TestData.ADMIN_ROLE);
    }

}
